var dssv = [];
var DSSV = "DSSV";
var dssvJson = localStorage.getItem(DSSV);
  if (dssvJson != null) {
    var svArr = JSON.parse(dssvJson);
    dssv = svArr.map(function (item) {
      return new SinhVien(
        item.maSV,
        item.tenSV,
        item.emailSV,
        item.passSV,
        item.diemToan,
        item.diemLy,
        item.diemHoa,
      );
    });
  }
  renderds(dssv);

function themSinhVien() {
  var sv = layThongTinForm();
  dssv.push(sv);
  var dssvJson = JSON.stringify(dssv)
  localStorage.setItem(DSSV,dssvJson)  
  renderds(dssv);
}
